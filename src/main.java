import java.util.Random;

public class main {

  public static void main(String[] args) {
    Monde monde = Monde.getInstance();
    Random gen = new Random();
    Biome[][] biomes = new Biome[14][9];


    for(int x=0;x<14;x++){
      for(int y=0;y<9;y++){
        biomes[x][y] = new Prairie(x,y);
      }
    }

    for(int x=0;x<14;x++){
      for(int y=0;y<9;y++){
        biomes[x][y].ajouterVoisin(biomes[x][y]);
        if(x != 0 && x != 13){
          if(y !=0 && y != 8){
            biomes[x][y].ajouterVoisin(biomes[x-1][y]);
            biomes[x][y].ajouterVoisin(biomes[x+1][y]);
            biomes[x][y].ajouterVoisin(biomes[x][y-1]);
            biomes[x][y].ajouterVoisin(biomes[x][y+1]);
            biomes[x][y].ajouterVoisin(biomes[x-1][y-1]);
            biomes[x][y].ajouterVoisin(biomes[x-1][y+1]);
            biomes[x][y].ajouterVoisin(biomes[x+1][y-1]);
            biomes[x][y].ajouterVoisin(biomes[x+1][y+1]);
          }
          else if(y == 8){
            biomes[x][y].ajouterVoisin(biomes[x-1][y]);
            biomes[x][y].ajouterVoisin(biomes[x+1][y]);
            biomes[x][y].ajouterVoisin(biomes[x][y-1]);
            biomes[x][y].ajouterVoisin(biomes[x-1][y-1]);
            biomes[x][y].ajouterVoisin(biomes[x+1][y-1]);
          }
          else{
            biomes[x][y].ajouterVoisin(biomes[x-1][y]);
            biomes[x][y].ajouterVoisin(biomes[x+1][y]);
            biomes[x][y].ajouterVoisin(biomes[x][y+1]);
            biomes[x][y].ajouterVoisin(biomes[x-1][y+1]);
            biomes[x][y].ajouterVoisin(biomes[x+1][y+1]);
          }
        }
        else if(x == 0){
          if(y !=0 && y != 8){
            biomes[x][y].ajouterVoisin(biomes[x+1][y]);
            biomes[x][y].ajouterVoisin(biomes[x][y-1]);
            biomes[x][y].ajouterVoisin(biomes[x][y+1]);
            biomes[x][y].ajouterVoisin(biomes[x+1][y-1]);
            biomes[x][y].ajouterVoisin(biomes[x+1][y+1]);
          }
          else if(y == 8){
            biomes[x][y].ajouterVoisin(biomes[x+1][y]);
            biomes[x][y].ajouterVoisin(biomes[x][y-1]);
            biomes[x][y].ajouterVoisin(biomes[x+1][y-1]);
          }
          else{
            biomes[x][y].ajouterVoisin(biomes[x+1][y]);
            biomes[x][y].ajouterVoisin(biomes[x][y+1]);
            biomes[x][y].ajouterVoisin(biomes[x+1][y+1]);
          }
        }
        else{
          if(y !=0 && y != 8){
            biomes[x][y].ajouterVoisin(biomes[x-1][y]);
            biomes[x][y].ajouterVoisin(biomes[x][y-1]);
            biomes[x][y].ajouterVoisin(biomes[x][y+1]);
            biomes[x][y].ajouterVoisin(biomes[x-1][y-1]);
            biomes[x][y].ajouterVoisin(biomes[x-1][y+1]);
          }
          else if(y == 8){
            biomes[x][y].ajouterVoisin(biomes[x-1][y]);
            biomes[x][y].ajouterVoisin(biomes[x][y-1]);
            biomes[x][y].ajouterVoisin(biomes[x-1][y-1]);
          }
          else{
            biomes[x][y].ajouterVoisin(biomes[x-1][y]);
            biomes[x][y].ajouterVoisin(biomes[x][y+1]);
            biomes[x][y].ajouterVoisin(biomes[x-1][y+1]);
          }
        }
        if(gen.nextFloat() > 0.40){
          if(gen.nextBoolean()){
            Lapin l = new Lapin(biomes[x][y], 1);
            biomes[x][y].ajouterAnimal(l);
            monde.ajoutAnimal(l);
          }
          else{
            Lapin l = new Lapin(biomes[x][y], 0);
            biomes[x][y].ajouterAnimal(l);
            monde.ajoutAnimal(l);
          }
        }
        if(gen.nextFloat() > 0.90){
          if(gen.nextBoolean()){
            Renard r = new Renard(biomes[x][y], 1);
            biomes[x][y].ajouterAnimal(r);
            monde.ajoutAnimal(r);
          }
          else{
            Renard r = new Renard(biomes[x][y], 0);
            biomes[x][y].ajouterAnimal(r);
            monde.ajoutAnimal(r);
          }
        }
        monde.addBiome(biomes[x][y]);
      }
    }
    monde.start();
  }
}
