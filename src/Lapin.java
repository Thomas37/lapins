import java.util.Random;
import java.util.ArrayList;

public class Lapin extends Animal{

  public Lapin(Biome b, Integer sexe){
    super(b,sexe);
    this.valeurNutritive = 3;
  }

  public Integer getNourriture(Biome b){
    Integer cpt = 0;
    for(Vegetal v : b.getVegetaux()){
      if(v instanceof Carotte){
        cpt += v.getValeurNutritive();
      }
    }
    return cpt;
  }

  @Override
  public void seFairePrendre() {
    this.energie -=2;
  }

  public void seReproduir(){
    this.energie -=2;
    for(Animal a : this.position.getAnimaux()){
      if(a instanceof Lapin && ((this.sexe == 0 && a.getSexe()==1) || (this.sexe == 1 && a.getSexe()==0))){
        a.seFairePrendre();
        Random gen = new Random();
        int sexe = 0;
        if(gen.nextBoolean()){
          sexe = 1;
        }
        Lapin l = new Lapin(this.position, sexe);
        Monde.getInstance().ajoutAnimal(l);
        this.position.ajouterAnimal(l);
        break;
      }
    }
  }

  public int getPartenaires(Biome b){
    int res = 0;
    for(Animal a : b.getAnimaux()){
      if(a instanceof Lapin && ((this.sexe == 0 && a.getSexe()==1) || (this.sexe == 1 && a.getSexe()==0))){
        res +=1;
      }
    }
    return res;
  }

  @Override
  public void manger() {
    if(this.getNourriture(this.position)>0){
      Vegetal c = new Carotte();
      for(Vegetal v : this.position.getVegetaux()){
        if(v instanceof Carotte){
          c = v;
          break;
        }
      }
      this.energie += c.getValeurNutritive();
      this.position.enleverVegetal(c);
    }
  }
}
