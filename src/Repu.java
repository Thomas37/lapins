import java.util.ArrayList;

public class Repu implements IEtat {
  public void jouerTour(Animal a){
    ArrayList<Biome> voisins = a.getPosition().getVoisins();
    Biome nouvPos = voisins.get(0);
    for(Biome b : voisins){
      if(a.getPartenaires(b) >= a.getPartenaires(nouvPos)){
        nouvPos = b;
      }
    }
    try{
    a.seDeplacer(nouvPos);
    }
    catch(Exception e){e.printStackTrace();}
    a.seReproduir();
    if(a.getEnergie()<3){
      a.changerEtat(new Affame());
    }
  }
}
