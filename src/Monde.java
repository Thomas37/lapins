import javax.swing.JFrame;
import java.awt.*;
import java.util.ArrayList;
import javax.swing.JFrame;
import java.util.concurrent.TimeUnit;

public class Monde extends JFrame{

  private ArrayList<Animal> animaux;
  private static Monde monde;
  private ArrayList<Biome> biomes;

  private Monde(){
    super();
    this.setTitle("Prairie Simulator");
    this.setSize(1500, 1000);
    this.setLocationRelativeTo(null);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.setVisible(true);
    this.animaux = new ArrayList<Animal>();
    this.biomes = new ArrayList<Biome>();
  }

  public void enleverAnimal(Animal a){
    this.animaux.remove(a);
  }

  public void ajoutAnimal(Animal a){
    this.animaux.add(a);
  }

  public static Monde getInstance() {
    if(Monde.monde == null){
      Monde.monde = new Monde();
    }
    return Monde.monde;
  }

  public void addBiome(Biome b){
    this.biomes.add(b);
    this.add(b);
    this.validate();
    this.repaint();
    this.setVisible(true);
  }

  @SuppressWarnings("unchecked")
  public void start(){
    for(int i=0;i<100000;i++){
      for(Biome b : this.biomes){
        b.jouerTour();
      }
      ArrayList<Animal> morts = new ArrayList<Animal>();
      ArrayList<Animal> tmp = (ArrayList<Animal>) this.animaux.clone();
      for(Animal a : tmp){
        if(a instanceof Lapin){
          a.jouerTour();
          if(a.getEnergie() <1){
            morts.add(a);
          }
        }
      }
      for(Animal a : tmp){
        if(a instanceof Renard){
          a.jouerTour();
          if(a.getEnergie() <1){
            morts.add(a);
          }
        }
      }
      for(Animal a : morts){
        this.enleverAnimal(a);
        a.getPosition().enleverAnimal(a);
      }
      this.repaint();
      try {
        TimeUnit.MILLISECONDS.sleep(2000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

}
