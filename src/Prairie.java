import java.util.ArrayList;

public class Prairie extends Biome {

  public Prairie(int x, int y){
    super(x,y);
    this.coutDeplacement = 1;
  }

  public void jouerTour(){
    super.jouerTour();
    this.ajouterVegetal(new Carotte());
  }

}
