import java.util.ArrayList;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.concurrent.TimeUnit;
import javax.imageio.ImageIO;

public abstract class Biome extends JComponent{

  protected Integer coutDeplacement;
  protected ArrayList<Animal> animaux;
  protected ArrayList<Vegetal> vegetaux;
  protected ArrayList<Biome> voisins;
  protected int height;
  protected int width;
  protected int x;
  protected int y;
  protected boolean animation;

  public Biome(int x, int y){
    coutDeplacement = 0;
    animaux = new ArrayList<Animal>();
    vegetaux = new ArrayList<Vegetal>();
    voisins = new ArrayList<Biome>();
    this.height = 100;
    this.width = 100;
    this.x = x;
    this.y = y;
    this.animation = true;
  }

  public ArrayList<Biome> getVoisins(){
    return this.voisins;
  }

  public Integer getCoutDeplacement(){
    return this.coutDeplacement;
  }

  public ArrayList<Animal> getAnimaux(){
    return this.animaux;
  }

  public ArrayList<Vegetal> getVegetaux(){
    return this.vegetaux;
  }

  public void jouerTour(){
  }

  public void enleverAnimal(Animal a){
    this.animaux.remove(a);
  }

  public void enleverVegetal(Vegetal v){
    this.vegetaux.remove(v);
  }

  public void ajouterAnimal(Animal a){
    this.animaux.add(a);
  }

  public void ajouterVegetal(Vegetal v){
    this.vegetaux.add(v);
  }

  public void ajouterVoisin(Biome b){
    this.voisins.add(b);
  }

  public void paintComponent(Graphics g){
    g.setColor(new Color(0,172,40));
    g.fillRect(this.x*(this.width+1)+10, this.y*(this.height+1)+10, this.width, this.height);
    BufferedImage img;
    int cpt = 0;
    for(Vegetal v : this.vegetaux){
      if(v instanceof Carotte) {
        if ((cpt / (this.height / 10)) * 10 >= 100) {
          break;
        }
        try {
          img = ImageIO.read(new File("res/textures/carrot.png"));
          g.drawImage(img, this.x * (this.width + 1) + 10 + (10 * (cpt % (this.width / 10))), this.y * (this.height + 1) + 10 + (10 * (cpt / (this.height / 10))), null);
          cpt++;
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
    cpt = 0;
    for(Animal a : this.animaux){
      if(a instanceof Lapin) {
        if ((cpt / (this.height / 10)) * 10 >= 100) {
          break;
        }
        try {
          if(this.animation) {
            if(a.getEtatCourant() instanceof Enfant){
              img = ImageIO.read(new File("res/entity/lapinEnfant1.png"));
            }
            else if(a.getEtatCourant() instanceof Repu){
              img = ImageIO.read(new File("res/entity/lapinRepu1.png"));
            }
            else{
              img = ImageIO.read(new File("res/entity/lapin1.png"));
            }
          }
          else{
            if(a.getEtatCourant() instanceof Enfant){
              img = ImageIO.read(new File("res/entity/lapinEnfant2.png"));
            }
            else if(a.getEtatCourant() instanceof Repu){
              img = ImageIO.read(new File("res/entity/lapinRepu2.png"));
            }
            else{
              img = ImageIO.read(new File("res/entity/lapin2.png"));
            }
          }
          g.drawImage(img, this.x * (this.width + 1) + 10 + (10 * (cpt % (this.width / 10))), this.y * (this.height + 1) + 10 + (10 * (cpt / (this.height / 10))), null);
          cpt++;
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
      else if(a instanceof Renard) {
        if ((cpt / (this.height / 10)) * 10 >= 100) {
          break;
        }
        try {
          if(this.animation) {
            if(a.getEtatCourant() instanceof Enfant){
              img = ImageIO.read(new File("res/entity/renardEnfant1.png"));
            }
            else if(a.getEtatCourant() instanceof Repu){
              img = ImageIO.read(new File("res/entity/renardRepu1.png"));
            }
            else{
              img = ImageIO.read(new File("res/entity/renard1.png"));
            }
          }
          else{
            if(a.getEtatCourant() instanceof Enfant){
              img = ImageIO.read(new File("res/entity/renardEnfant2.png"));
            }
            else if(a.getEtatCourant() instanceof Repu){
              img = ImageIO.read(new File("res/entity/renardRepu2.png"));
            }
            else{
              img = ImageIO.read(new File("res/entity/renard2.png"));
            }
          }
          g.drawImage(img, this.x * (this.width + 1) + 10 + (10 * (cpt % (this.width / 10))), this.y * (this.height + 1) + 10 + (10 * (cpt / (this.height / 10))), null);
          cpt++;
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
    this.animation = !this.animation;
  }
}
