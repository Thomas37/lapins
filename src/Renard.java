import java.util.Random;

public class Renard extends Animal {

  public Renard(Biome b,Integer sexe) {
    super(b, sexe);
    this.energie = 10;
  }

  public Integer getNourriture(Biome b){
    Integer cpt = 0;
    for(Animal m : b.getAnimaux()){
      if(m instanceof Lapin){
        cpt += m.getValeurNutritive();
      }
    }
    return cpt;
  }

  public void seReproduir(){
    this.energie -=3;
    for(Animal a : this.position.getAnimaux()){
      if(a instanceof Renard && ((this.sexe == 0 && a.getSexe()==1) || (this.sexe == 1 && a.getSexe()==0))){
        a.seFairePrendre();
        Random gen = new Random();
        int sexe = 0;
        if(gen.nextBoolean()){
          sexe = 1;
        }
        Renard r = new Renard(this.position, sexe);
        Monde.getInstance().ajoutAnimal(r);
        this.position.ajouterAnimal(r);
        break;
      }
    }
  }

  public int getPartenaires(Biome b){
    int res = 0;
    for(Animal a : b.getAnimaux()){
      if(a instanceof Renard && ((this.sexe == 0 && a.getSexe()==1) || (this.sexe == 1 && a.getSexe()==0))){
        res +=1;
      }
    }
    return res;
  }

  @Override
  public void manger() {
    if(this.getNourriture(this.position)>0){
      Animal l = new Lapin(this.position, 0);
      for(Animal a : this.position.getAnimaux()){
        if(a instanceof Lapin){
          l = a;
          break;
        }
      }
      this.energie += l.getValeurNutritive();
      this.position.enleverAnimal(l);
      Monde.getInstance().enleverAnimal(l);
    }
  }
}
