import java.util.ArrayList;

public class Enfant implements IEtat {

  public void jouerTour(Animal a) throws BiomePasVoisinException{
    if(a.getNourriture(a.getPosition())<5){
      ArrayList<Biome> voisins = a.getPosition().getVoisins();
      Biome nouvPos = voisins.get(0);
      for (Biome b : voisins) {
        if (a.getNourriture(b) > a.getNourriture(nouvPos)) {
          nouvPos = b;
        }
      }
      if(a.getNourriture(nouvPos) == a.getNourriture(a.getPosition())){

      }
      else{
        a.seDeplacer(nouvPos);
      }
    }
    a.manger();
    if(a.getAge()>3){
      a.changerEtat(new Affame());
    }
  }
}
