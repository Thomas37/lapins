import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public abstract class Animal{

  protected Integer energie;
  protected Integer energieMax;
  protected Integer valeurNutritive;
  protected Integer sexe;
  protected Integer age;
  public static Integer ageMoyen;
  protected Boolean vivant;
  protected Biome position;
  protected IEtat etatCourant;

  public Animal(Biome b, Integer sexe){
    this.energie = 1;
    this.valeurNutritive = 2;
    this.etatCourant = new Enfant();
    this.position = b;
    this.age = 0;
    this.sexe = sexe;
  }

  public Integer getAge(){
    return this.age;
  }

  public Integer getSexe(){
    return this.sexe;
  }

  public int getPartenaires(Biome b){
    return 0;
  }

  public void seFairePrendre(){
    this.energie -=3;
  }

  public void seReproduir() {
  }

  public void changerEtat(IEtat e) {
    this.etatCourant = e;
  }

  public void jouerTour() {
    this.energie -=1;
    this.age +=1;
    try {
      this.etatCourant.jouerTour(this);
    } catch (BiomePasVoisinException e) {
      e.printStackTrace();
    }
  }

  public Biome getPosition(){
    return this.position;
  }

  public void seDeplacer(Biome b) throws BiomePasVoisinException{
    if(this.position.getVoisins().contains(b)){
      this.position.enleverAnimal(this);
      b.ajouterAnimal(this);
      this.position = b;
      this.energie -=b.getCoutDeplacement();
    }
    else{
      throw new BiomePasVoisinException();
    }
  }

  public Integer getEnergie(){
    return this.energie;
  }

  public IEtat getEtatCourant() {
    return etatCourant;
  }

  public Integer getNourriture(Biome b){
    return 0;
  }
  public void manger(){}

  public Integer getValeurNutritive() {
    return valeurNutritive;
  }
}
